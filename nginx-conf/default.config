server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl default_server;
    listen [::]:443 ssl default_server;
    server_name  localhost;

    ssl on;
    ssl_certificate /etc/nginx/nginx.crt;
    ssl_certificate_key /etc/nginx/nginx.key;

    access_log  /var/log/nginx/host.access.log  main;

    location / {
        proxy_set_header    X-Forwarded-For $remote_addr;
        proxy_pass          http://node-retdev:3000/;
    }

    location /public {
        proxy_set_header    X-Forwarded-For $remote_addr;
        proxy_pass          http://node-retdev:3000/public;
    }

    location /private {
        proxy_set_header    X-Forwarded-For $remote_addr;
        proxy_pass          http://node-retdev:3000/private;

        #add auth_basic on private
        auth_basic "Restricted";
        auth_basic_user_file /etc/nginx/.htpasswd;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    location ~ /\.ht {
        deny  all;
    }
}
