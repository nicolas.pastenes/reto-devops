FROM node:14
#Descarga imagen de dockerhub con 
#contenidido ejecutable para version node de este proyecto actual
#nose -v = 14


#creacion de usuario diferente a root en este caso el mio
#deshabilitacion de password
RUN adduser -disabled-password --gecos '' node

USER node

WORKDIR /app/node-retdev
#directorio dentro de nuestro contenedor donde sera
#cargado el proyecto

COPY package*.json ./
#copia de todos los archivos que contengan el nombre package

RUN npm install


#ejecucion de npm para instalar dependencias
COPY . .
COPY --chown=node:node . .
#copia de todos los archivos
EXPOSE 3000
#puerto asignado a aplicacion node js de docker
CMD ["node","./index.js"]
#ejecucion de comando con parametros. ejecutara node con el archivo index.js
